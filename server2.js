const express= require('express');
var Pool= require('pg').Pool;
var bodyParser= require('body-parser');
var dateFormat= require('dateformat');


const app= express();
var config= {
	host: 'localhost',
	user: 'manager',
	password: 'g~N]mD:>(9y2A,5S',
	database: 'sever2',
};

var pool= new Pool(config);

app.set('port', (8080));
app.use(bodyParser.json({type: 'application/json'}));
app.use(bodyParser.urlencoded({extended: true}));

app.get('/list-workshops', async (req, res) => {
	var request= req.query.workshops;
	try{
		var response= await pool.query('select title, date, location from workshops')
		if (request == undefined){
			res.json({workshops: response.rows.map(function (item){
				return {title: item.title , date: dateFormat(item.date, 'yyyy-mm-dd'), location: item.location};
			})});
		}
	}
	catch(e){
		console.error('Error running query '+ e);
	}
});

app.get('/list-users', async (req, res) => {
	var request= req.query.type;
	try{
		var response= await pool.query('select distinct firstname, lastname from users')
		var response2= await pool.query('select distinct firstname, lastname, username, email from users')
		if (request == undefined){
			res.json({error: 'parameters not given'});
		}
		else if (request == 'summary'){
			res.json({users: response.rows});
		}
		else if(request == 'full'){
			res.json({users: response2.rows});
		}
	}
	catch(e){
		console.error('Error running query '+ e);
	}
});

app.get('/attendees', async (req, res) => {
	var title= req.headers.title;
	var date= req.headers.date;
	var location= req.headers.location;
	try{
		if (title == undefined || date == undefined || location == undefined){
			res.json({error: 'Please enter a title, date, and location of the course that you are looking for'});
		}
		else{
			var response= await pool.query('select firstname, lastname from users join attending on users.userID= attending.userID where attending.workshopID in (select workshopID from workshops where title=$1 and date=$2 and location=$3);', [title, date, location]);
			res.json({attendees: response.rows}); 
		}

	}
	catch(e){
		console.error('Error running query '+ e);
	}
});

app.get('/non-attendees', async (req, res) =>{
	var title= req.headers.title;
	var date= req.headers.date;
	var location= req.headers.location;
	try{
		if (title == undefined || date == undefined || location == undefined){
			res.json({error: 'Please enter a title, date, and location the course that you are looking for'});
		}
		else{
			var response= await pool.query('select firstname, lastname from users join attending on users.userID= attending.userID where attending.userID not in (select distinct userID from attending join workshops on workshops.workshopID= attending.workshopID where workshops.title=$1 and workshops.date=$2 and workshops.location=$3);', [title, date, location]);
			res.json({non_attendees: response.rows}); 
		}

	}
	catch(e){
		console.error('Error running query '+ e);
	}
});

app.post('/create-user', async (req, res) => {
	var firstname= req.body.firstname;
	var lastname= req.body.lastname;
	var username= req.body.username;
	var email= req.body.email;

	if(!firstname || !lastname || !username || !email){
			res.json({error: 'parameters not given'});
	}
	else{
	    try{
	    	var exists= await pool.query('select username from users where username=$1',[username]);
	    	if (exists.rowCount <1){
	    		var response = await pool.query('insert into users (firstname, lastname, username, email) values ($1,$2,$3,$4);',[firstname,lastname,username,email]);
	    		res.json({status: 'user added'});
	    	}else {
	    		res.json({status: 'username taken'});
	    	}
	    }
	    catch(e){
	    	console.error('Error running query '+ e);
	    }
	}
});

app.post('/add-workshop', async (req, res) => {
	var title= req.body.title;
	var date= req.body.date;
	var location= req.body.location;
	var maxseats= req.body.maxseats;
	var instructor= req.body.instructor;

	if(!title | !date | !location | !instructor | !maxseats){
		res.json({error: 'parameters not given'});
	}else{
		try{
			var check= await pool.query('select workshopID from workshops where title=$1 and date=$2 and location=$3 and instructor=$4 and maxseats=$5;',[title,date,location,instructor,maxseats]);
			if (check.rowCount > 0){
				res.json({status: 'workshop already in datebase'});
			}else{
				var response= await pool.query('insert into workshops (title, date, location, instructor, maxseats) values ($1,$2,$3,$4,$5);',[title,date,location,instructor,maxseats]);
				res.json({status: 'workshop added'});
			}
		}catch(e){
			console.error('Error running query '+e);
		}
	}
});

app.post('/enroll',async (req, res) => {
	var username= req.body.username;
	var title= req.body.title;
	var date= req.body.date;
	var location= req.body.location;
	if(!username || !title || !date || !location){
		res.json({error: 'parameters not given'});
	}else{
	    try{
	    	checkWorkshop= await pool.query('select workshopID from workshops where title=$1 and date=$2 and location=$3;',[title,date,location]);
	    	checkUser= await pool.query('select userID from users where username=$1;',[username]);
	    	if (checkWorkshop.rowCount<1){
	    		res.json({status: 'workshop does not exist'});
	    	}else{
	    		if(checkUser.rowCount<1) {
	    			res.json({status: 'user does not exist'});
	    		}else{
	    			var inclass= await pool.query('select userID from attending where userID=(select userID from users where username=$1) and workshopID=(select workshopID from workshops where title=$2 and date=$3 and location=$4);',[username,title,date,location]);
	    			if(inclass.rowCount>0){
	    				res.json({status: 'user is already enrolled'});
	    			}else{
	    			    var max= await pool.query('select maxseats from workshops where title=$1 and date=$2 and location=$3;',[title,date,location]);
	    			    var taken= await pool.query('select enrolled from workshops where title=$1 and date=$2 and location=$3;',[title,date,location]);
	    			    if(max.rows[0].maxseats > taken.rows[0].enrolled){
	    			    	var updateEnroll= await pool.query('update workshops set enrolled=enrolled+1 where title=$1 and date=$2 and location=$3;',[title,date,location]);
	    			    	var add= await pool.query('insert into attending (userID, workshopID) values ((select userID from users where username=$1),(select workshopID from workshops where title=$2 and date=$3 and location=$4))', [username,title,date,location]);
	    			    	res.json({status: 'user added'});
	    			    }else{
	    			    	res.json({status: 'no seats available'});
	    			    }
	    			}    
	    		}
	    	}
	    }catch(e) {
	    	res.json('Error running query '+e);
	    }
	}
});

app.delete('/unenroll', async (req, res) => {
	var username= req.body.username;
	var title= req.body.title;
	var date= req.body.date;
	var location= req.body.location;
	if(!username || !title || !date || !location){
		res.json({error: 'parameters not given'});
	}else{
		try{
		    checkWorkshop= await pool.query('select workshopID from workshops where title=$1 and date=$2 and location=$3;',[title,date,location]);
	        checkUser= await pool.query('select userID from users where username=$1;',[username]);
	        if (checkWorkshop.rowCount<1){
	        	res.json({status: 'workshop does not exist'});
	        }else{
	        	if(checkUser.rowCount<1) {
	    			res.json({status: 'user does not exist'});
	    		}else{
	    			var inclass= await pool.query('select userID from attending where userID=(select userID from users where username=$1) and workshopID=(select workshopID from workshops where title=$2 and date=$3 and location=$4);',[username,title,date,location]);
	    			if(inclass.rowCount<1){
	    				res.json({status: 'user is not enrolled'});
	    			}else{
	    				var updateEnroll= await pool.query('update workshops set enrolled=enrolled-1 where title=$1 and date=$2 and location=$3;',[title,date,location]);
	    			    var add= await pool.query('delete from attending where userID=(select userID from users where username=$1) and workshopID=(select workshopID from workshops where title=$2 and date=$3 and location=$4);', [username,title,date,location]);
	    			    res.json({status: 'user unenrolled'});
	    			}
	    		}
	        }
		}catch(e){
			res.json('Error running query '+e);
		}
	}
});

app.delete('/delete-user', async (req, res) => {
	var request= req.query.username;
	try{
		var response= await pool.query('select username from users;');
		var usernames= response.rows.map(function (item) {
			return item.username;
		});
		if (request== undefined){
			res.json({usernames: usernames});
		}else{
			var response2= await pool.query('select username from users where username=$1',[request]);
			if (response2.rowCount <1) {
				res.json({status: 'username not found'});
			}else{
				var updateseats= await pool.query('update workshops w set enrolled= enrolled-1 from attending join users on attending.userID= users.userID where w.workshopID=attending.workshopID and attending.userID= users.userID and users.username=$1;',[request]);
				var deleteClass= await pool.query('delete from attending where userID=(select userID from users where username=$1);',[request]);
				var deleteUser= await pool.query('delete from users where username=$1',[request]);
				res.json({status: "deleted"});
			}
		}
	}catch (e){
		console.error('Error running query '+ e);
	}
});

app.delete('/delete-workshop', async (req,res) => {
	var title= req.body.title;
	var date= req.body.date;
	var location= req.body.location;
	if(!title || !date || !location){
		res.json({error: 'parameters not given'});
	}else{
		try{
			var checkWorkshop= await pool.query('select workshopID from workshops where title=$1 and date=$2 and location=$3;',[title,date,location]);
	        if (checkWorkshop.rowCount<1){
	        	res.json({status: 'workshop does not exist'});	
	        }else{
	        	var deleteworkshop1= await pool.query('delete from attending where workshopID=(select workshopID from workshops where title=$1 and date=$2 and location=$3);',[title,date,location]);
	        	var deleteworkshop2= await pool.query('delete from workshops where workshopID=(select workshopID from workshops where title=$1 and date=$2 and location=$3);',[title,date,location]);
	        	res.json({status: 'workshop deleted'});
	        }
		}catch(e){
			console.error('Error running query '+e);
		}
	}
});

app.listen(app.get('port'), () => {
	console.log('Running');
})













