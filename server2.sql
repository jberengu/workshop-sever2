drop database if exists sever2;
create database sever2;
\c sever2

drop role if exists manager;
create user manager with password 'g~N]mD:>(9y2A,5S';

drop table if exists workshops;
drop table if exists users;

create table workshops (
	workshopID serial PRIMARY KEY,
	title text,
	location text,
	date date,
	maxseats int,
	enrolled int not null default 0,
	instructor text
);

create table users (
	userID serial PRIMARY KEY,
	firstname text,
	lastname text,
	username text,
	email text
);

create table attending (
	userID int REFERENCES users(userID),
	workshopID int REFERENCES workshops(workshopID),
	PRIMARY KEY (userID, workshopID)
);

insert into workshops (title, location, date, maxseats, enrolled, instructor) values
	('React Fundamentals', 'Boston', '2017-09-25', 20, 6, 'somebody'),
	('React Fundamentals', 'Boston', '2018-02-05', 25, 2, 'somebody else'),
	('React Fundamentals', 'Richmond', '2017-09-27', 20, 1, 'somebody else2'),
	('Machine Learning', 'Portland', '2018-02-14', 15, 1, 'somebody else3');

insert into users (firstname, lastname, username, email) values
	('Ann', 'Mulkern', 'ann', 'ann.mulkern@gmail.com'),
	('Ben', 'Nowicki', 'bnowicki', 'benben@umw.edu'),
	('Jim', 'Morgna', 'jmorgna', 'jmorgna@umw.edu'),
	('Clara', 'Weick', 'cweick', 'cweick@umw.edu'),
	('Mary', 'Yung', 'myung', 'myung@umw.edu'),
	('Ahmed', 'Abdelali', 'abdel', 'abdel@umw.edu');

insert into attending (userID, workshopID) values
	(1,1),
	(2,1),
	(2,2),
	(3,1),
	(4,1),
	(5,1),
	(6,1),
	(6,2),
	(6,3),
	(6,4);

grant all on workshops,users,attending to manager;
grant usage, select on all sequences in schema public to manager;


